// PIC12F675 Configuration Bit Settings
//
// CONFIG
#pragma config FOSC = INTRCIO   // Oscillator Selection bits (INTOSC oscillator: I/O function on GP4/OSC2/CLKOUT pin, I/O function on GP5/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = ON       // Power-Up Timer Enable bit (PWRT enabled)
#pragma config MCLRE = OFF      // GP3/MCLR pin function select (GP3/MCLR pin function is digital I/O, MCLR internally tied to VDD)
#pragma config BOREN = OFF      // Brown-out Detect Enable bit (BOD disabled)
#pragma config CP = OFF         // Code Protection bit (Program Memory code protection is disabled)
#pragma config CPD = OFF        // Data Code Protection bit (Data memory code protection is disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
// your configuration bit settings go here
// configuration code (indicated earlier) omitted for brevity

#define GPIO0_SP1    GPIO0 // In   pin7
#define GPIO1_nRST   GPIO1 // Out  pin6
#define GPIO3_psbDW  GPIO2 // In   pin5
#define GPIO2_psbUP  GPIO3 // In   pin4
#define GPIO4_WDrst  GPIO4 // In   pin3
#define GPIO5_SP2    GPIO5 // In   pin2

#define TRIS0_SP1    TRISIO0 // In 
#define TRIS1_nRST   TRISIO1 // Out
#define TRIS3_psbDW  TRISIO2 // In 
#define TRIS2_psbUP  TRISIO3 // In
#define TRIS4_WDrst  TRISIO4 // In
#define TRIS5_SP2    TRISIO5 // In

#define SW_IDLE 3
#define SW_UP   2
#define SW_DW   1
#define SW_STOP 0

#define CNTSW_IDL 0
#define CNTSW_SET 3
#define CNTSW_ONE 2
#define CNTSW_END 1

#define RUN_UP    2
#define RUN_DW    1
#define RUN_STOP  0

#define CNTREED_ACT  50
#define CNTREED_END  250
#define CNTREED_INIT 150

#define PIN_ACTIVE  0b0
#define PIN_NOTACT  0b1

#define SET_OUTPUT  0b0
#define SET_INPUT   0b1

#define CNTRSTval    10*100 // Secondi*100
#define CNTWDval     5*100  // Secondi*100
#define CNTRSTPLSval 1*100  // Secondi*100

#define END_POSIT   20

#define CMCON_DISABLE 0x07

#define ANSEL_DISABLE 0x00

typedef unsigned char byte;

// Proto
void  SwitchHandler   (void);
void  ReedEMUHandler  (void);
void  WatchDogHandler (void);
byte  GPIOwrite       (byte Pin, byte Status);
byte  GPIOread        (byte Pin);
void  SETpinRST       (int);

unsigned int lastGPIO4_WDrst;

// Variables
unsigned int cntRST;
unsigned int cntWD;
unsigned int cntRSTpls;

byte GPIO4mirror;
byte lastGPIO4mirror;
const byte maskGPIO4=0x10;

void interrupt myIsr(void)
{
// Only process timer-triggered interrupts
    if(INTCONbits.TMR0IE && INTCONbits.TMR0IF) 
    {
        INTCONbits.TMR0IF = 0b0; // clear this interrupt condition
        TMR0 = 156;              // Divider to 10ms (1MHz/64/156=100Hz)
// -----------------------------------------------------------------------------
		if(cntRSTpls>0)
        {
            cntRST = CNTRSTval;
            cntWD  = CNTWDval;
            cntRSTpls--
        }
        else
        {
            if((GPIO.GPIO2_psbUP==PIN_ACTIVE) && (GPIO.GPIO3_psbDW==PIN_ACTIVE)) 
            {
                if (cntRST>0) cntRST--;
            }
            else 
            {
                cntRST=CNTRSTval;
            }

            if(GPIO.GPIO4_WDrst!=lastGPIO4_WDrst) 
            {
                cntWD = CNTWDval;
            }
            else
            {
                if(cntWD>0) cntWD--;
            }

            lastGPIO4mirror=GPIO.GPIO4_WDrst;

            if((cntWD==0) || (cntRST==0)) 
            {
                SETpinRST(PIN_ACTIVE);
                cntWD = CNTWDval;
                cntRSTpls = CNTRSTPLSval;
            }
            else                      
                SETpinRST(PIN_NOTACT);
        }
    }
}

int main(void)
{

    CMCON = CMCON_DISABLE;
    
    ANSEL = ANSEL_DISABLE;
    
// Prescaler settings
    OPTION_REGbits.PSA  = 0b0;    // Prescaler is assigned to the Timer0 module
    OPTION_REGbits.PS   = 0b110;  // Prescaler Rate Select bits 1/128 128uS w 1uS CLKOUT
    OPTION_REGbits.T0CS = 0b0;    // Internal instruction cycle clock (CLKOUT)
//
    
    GPIO = 0xFF;
    
	TRIS0_SP1    = SET_INPUT ; 
	TRIS1_nRST   = SET_OUTPUT; 
	TRIS2_psbUP  = SET_INPUT ; 
	TRIS3_psbDW  = SET_INPUT ;
	TRIS4_WDrst  = SET_INPUT ;
	TRIS5_SP2    = SET_INPUT ;

    SETpinRST(PIN_NOTACT);
    
    GPIO4mirror = GPIO & maskGPIO4;
    
    cntWD     = CNTWDval;
    cntRST    = CNTRSTval;
    cntRSTpls = 0;
 
// Timer settings   
    TMR0 = 78;              // Divider to 10ms (1MHz/128/78=100Hz)
// Internal clock, no prescaler
    INTCONbits.TMR0IE = 0b1; // Enable interrupts for timer 0
    INTCONbits.GIE    = 0b1; // Enable interrupts

    while(1) ; 
}

void SETpinRST (int Status)
{
    if (Status) {GPIO.TRIS1_nRST = SET_INPUT;  GPIO.GPIO1_nRST = PIN_NOTACT;}
    else        {GPIO.GPIO1_nRST = PIN_ACTIVE; GPIO.TRIS1_nRST = SET_OUTPUT;}
}
