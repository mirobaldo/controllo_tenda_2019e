//----------------------------------------------------------------------------------------------------------------------
#define VERSION_ "20170921 v3.34.30"
//                        Improved of 3.00.00
//                        Code3: Implementazione settembre 2017
//                        Fix34: Modifiche 
//...............................................................
//                        Bug30: Correzioni e tempo accensione in settimane
//                        Bug28: #define dei DisplaySel
//                        Bug27: corretto bug temp
//                        Bug25: runtime
//                        Bug22: Debug                      
//                        Bug19: Tolta tabella movimento                      
//                        Bug18: Tolte subroutine allarme vento                      
//                        Bug17: Tolte subroutine controlla tenda                      
//                        Bug16: tolto Runtime                      
//                        Bug15: tentativo correzione bug Runtime                      
//                        Bug13: Corretto Runtime                      
//                        Bug12: Inserito Runtime                      
//                        Bug11: Corretto puntatore RotCH                      
//                        Bug10: Corretti dimensioni array                      
//                        Bug09: Test no display                      
//                        Bug08: cntWind diventa uint8_t                      
//                        Bug07: uint8_t in loop                      
//                        Bug06: ResetRequest linitato e uint16_t
//                        Bug05: Corretto errore conversione int
//                        Bug03: Tolti messaggi debug
//                        Bug02: Correzioni tickHour
//----------------------------------------------------
// Riassunto
//
// Interrupt pin 3 --- Anemometro
// - Set swWindAntiBump
//
// Interrupt timer
//
// # Routine 1/1000 di secondo
// - Routine antirimbalzo wind antibump
//   . inc cntWind se swWindAntibump=1
// - Test reed posizione
//  . set/reset impulso di reed
//
// # Routine 1/10 di secondo
// - Gestione pulsanti di controllo
//  . Gestione pulsante open,  CntSwOpen
//  . Gestione pulsante down CntSwDown
// - Gestione pulsanti
//  . Call SUNBSwitchElab
// - Controllo tenda
//  . Call SUNBCommandCtrl
//
// # Routine 1 secondo
// - Gestione messaggi di controllo su linea seriale
// - Gestione velocità vento
//  . Aggorna WindSpeed
// - Gestione controllo vento
//  . Call WindControl
// - Gestione display

#include <Timer.h>
//#include <Adafruit_SleepyDog.h>
//#include <EEPROM.h>

#define DISPLAYTEMP    1
#define DISPLAYHUMID   2
#define DISPLAYPOS     3
#define DISPLAYWSPEED  4
#define DISPLAYREALTIM 5

#define TESTMODE 0 // Se = 1, test mode, mettere a 0 se NON test
#define MonitorMODE 1 // Se = 1, test del vento, mettere a 0 se NON test
#define PlotterMODE 1 // Se = 1, test del vento, mettere a 0 se NON test

#if TESTMODE == 1
#define WINDTIMER    13   // Secondi. Valore timer di ripristino in caso di vento forte
#define WINDTIMERSTEP 1   // Secondi. Valore timer di ripristino in caso di vento forte
#define WINDALARM     2   // Velocità del vento per sollevare la tenda
#define RE_TIMEOUT 4000   // milliSeconds
#define BARDISPTIME   5   // secondi
#define DISP_INTERVAL 3590
#else
#define WINDTIMER     90  // Secondi. Valore timer di ripristino in caso di vento forte
#define WINDTIMERSTEP 10  // Secondi. Valore timer di ripristino in caso di vento forte
#define WINDALARM     14  // Velocità del vento per sollevare la tenda con P > 4
#define RE_TIMEOUT  4000  // milliSeconds
#define BARDISPTIME 3600  // secondi
#define DISP_INTERVAL 0
#endif

#define RE_BUMPFILTER_ACT    2
#define RE_BUMPFILTER_NOTACT 5

#define SWITCHTIMEFILTER 2   // *50mS
#define SWITCHTIMELONG   20  // *50mS
#define SWITCHTIMERESET  120 // *50mS

#define PIN_ACTIVE LOW
#define PIN_NOTACT HIGH
#define FLG_ACTIVE true
#define FLG_NOTACT false

#define pMD_UPSMALL 0
#define pMD_DWSMALL 1
#define pMD_UPLARGE 2
#define pMD_DWLARGE 3
#define pMD_DWwind  4
#define pMD_UPwind  5

Timer Tmr;

#define BARTEMP  'T'
#define BARHUMID 'H'
#define BARPOSIT 'P'
#define BARWIND  'W'

char DispString[20];
char BarSet;

// Prototipi di funzioni
void Int3_MeasureWind(void);
void SUNBSwitchElab(void);
void WindControl(void);
void SUNBCommandCtrl (void);
void PinSwitch(void);

void ReadTempHigro(void);

int  MatrixWriteChar(byte, int, int);
void CleanBarDisplay(void);
//void PrintByte(byte);
//void PrintLword(uint32_t);
void RotateCH(int, byte *);
int  MatrixWriteString(char*,int, bool);

void MatrixWriteBar(int8_t*, int, char);
int  FloatDivision(int, int);
void IntTim_ReadSwitch(void);

bool enMonitorMode = true;
bool enPlotterMode = false;

//-----------------------------------------------------------------------------------

uint8_t  timerWeeks   = 0;
uint8_t  timerDays    = 0;
uint8_t  timerHours   = 0;
uint8_t  timerMinutes = 0;
uint8_t  timerSeconds = 0;
uint16_t timerWindDelta = 0;
uint16_t timerHourTick      = 3600;
bool     tickHour       = true;

uint8_t CntSwOpen   = 0;
uint8_t CntSwDown   = 0;
uint8_t CntWdt      = 0;

uint8_t pinOpenOutHold = 1;
uint8_t pinDownOutHold = 1;

bool SendMsg = false;
bool SendPlot = false;
bool RoutineInLoop1sec = true;

int timWind;       // Timer di ripristino in caso di vento forte
int timWindMult = 0;

uint16_t ResetRequest = 0;
bool ResetRequestOnError = false;

#define CO_STARTUP_00         0 // Startup
#define CO_STARTUP_01         1 // Startup
#define CO_WAIT_02            2 // Idle, nulla attivo
#define CO_SUNBOPENON_03      3 //  Apri tenda, comandi OPEN=0 DOWN=1 (0=ON)
#define CO_SUNBDOWNON_04      4 // Chiudi tenda, comandi OPEN=1 DOWN=0 (0=ON)
#define CO_SUNBOPENACT_05     5 //  Resetta comando open, comandi OPEN=1 DOWN=1 (0=ON)
#define CO_SUNBDOWNACT_06     6 // Resetta comando down, comandi OPEN=1 DOWN=1 (0=ON)
#define CO_STOPMOTOR_07       7 //  Blocca tenda, comandi OPEN=0 DOWN=0 (0=ON)
#define CO_CLEARCOMMAND_08    8 // Resetta, comandi OPEN=1 DOWN=1 (0=ON)
#define CO_OVERHEATRESET_09   9 // Errore!

uint8_t smSUNBCommand = CO_STARTUP_00;

int8_t SUNBstep    = 0;
int8_t SUNBpos, meanSUNBpos;;
int8_t SUNBposHold = 0;

bool flgSUNBStop = false;
bool flgSUNBWait = false;
bool flgSUNBRun = false;

#define TIMEROVERHEAT 10*60*20;           // 10 minuti
uint16_t timeOutOverheat = TIMEROVERHEAT; // 1/20 sec, 20=1 secondo

bool StatusLED = false;

#define WA_NOALARM_00        0
#define WA_ALARM_10         10
#define WA_MOVETOZERO_01     1
#define WA_MOVETO1STSTEP_01
#define WA_MOVETO1STSTEP_02  2
#define WA_WAITENDALARM_03   3
#define WA_WAITENDRESTORE_04 4
#define WA_BUMPFILTER         3 // Valore antibump

uint8_t   smWindAlarm   = WA_NOALARM_00;
bool Stop_smWindAlarm   = false;

bool     flgReedStatus  = false;
bool     flgReedPulse   = false;
bool     flgReedPrintOnPulse = false;

uint16_t timReedOn      = 0;
uint16_t timReedOff     = 0;

uint16_t timReedLastOn  = 0;
uint16_t timReedLastOff = 0;
uint8_t  swReedAntiBumpACT  = RE_BUMPFILTER_ACT;
uint8_t  swReedAntiBumpNACT = RE_BUMPFILTER_NOTACT;

uint16_t cntRoutine50ms = 50;
uint16_t cntRoutine1000ms = 1000;

uint8_t swWindAntiBump = WA_BUMPFILTER;

#include "LedControl.h"

#include "DHT.h"
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
#define DHTTYPE DHT11   // DHT 22  (AM2302), AM2321

const byte pinWindSwitch_int =  3; // Anemometro rosso, giallo a massa

const byte pinOpenOut        =  8; // bianco
const byte pinDownOut        =  9; // verde

const byte pinOpenIn         =  6;
const byte pinDownIn         =  7;

const byte pinPosReed        =  4;

const byte pinDHT            =  5; // what digital pin we're connected to

const byte pinLCLoad         = 10; // LedControl load pin
const byte pinLClock         = 11; // LedControl clock pin
const byte pinLCData         = 12; // LedControl data pin

LedControl lc=LedControl(pinLCData,pinLClock,pinLCLoad,4);

DHT dht(pinDHT, DHTTYPE);

float Temperature, meanTemperature;
float Humidity,    meanHumidity;

uint8_t cntWind = 0;
uint16_t WindSpeed = 0, meanWindSpeed;

uint8_t  InitLoops = 100;

static  char inChar='0';

#define arraySize 14
#define arrayDIM  15

int8_t  arrayTemp[arrayDIM]  ={0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0}; // Controllati puntatori
int8_t  arrayHumid[arrayDIM] ={0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0}; // Controllati puntatori
int8_t  arrayWind[arrayDIM]  ={0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0}; // Controllati puntatori
int8_t  arrayPosit[arrayDIM] ={0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0}; // Controllati puntatori

const byte pinLED = 13; // LED connected to digital pin 13
#define SUNBMIN       0   // Giri
#define SUNBMAX       16  // Giri
#define SUNBOVER      20  // Giri
#define SUNBSTEP      4   // Giri
#define SUNBWINDPOS   4   // Giri -- Posizione tenda con vento.
#define SUNBMAXHOLD   15  // Giri -- Posizione tenda max se c'è un allarme vento
#define SUNBSTEPEXTRA 1   // Giri -- Extra step per errore ricarica
//
uint32_t MatrixArea[8];

// int arrayStart;
bool SUNBStart;
// int BarToDisplay;

const byte CH[][6] = {
//  -------------------------------------------
  {2, B00000000,  // 0x20_space ........   #0
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {1, B01011111,  // 0x21_!     .#.#####   #1
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {3, B00000011,  // 0x22_      ......##   #2
      B00000000,  //            ........
      B00000011,  //            ......##
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {5, B00010100,  // 0x23_#     ...#.#..   #3
      B00111110,  //            ..#####.
      B00010100,  //            ...#.#..
      B00111110,  //            ..#####.
      B00010100}, //            ...#.#..
//---------------------------------------------
  {4, B00100100,  // 0x24_$     ..#..#..   #4
      B01101010,  //            .##.#.#.
      B00101011,  //            ..#.#.##
      B00010010,  //            ...#..#.
      B00000000}, //            ........
//---------------------------------------------
  {5, B01100011,  // 0x25_%     .##...##   #5
      B00010011,  //            ...#..##
      B00001000,  //            ....#...
      B01100100,  //            .##..#..
      B01100011}, //            .##...##
//---------------------------------------------
  {5, B00110110,  // 0x26_&     ..##.##.   #6
      B01001001,  //            .#..#..#
      B01010110,  //            .#.#.##.
      B00100000,  //            ..#.....
      B01010000}, //            .#.#....
//---------------------------------------------
  {1, B00000011,  // 0x27_'     ......##   #7
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {3, B00011100,  // 0x28_(     ...###..   #8
      B00100010,  //            ..#...#.
      B01000001,  //            .#.....#
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {3, B01000001,  // 0x29_)     .#.....#   #9
      B00100010,  //            ..#...#.
      B00011100,  //            ...###..
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {5, B00101000,  // 0x2A_*     ..#.#...   #10
      B00011000,  //            ...##...
      B00001110,  //            ....###.
      B00011000,  //            ...##...
      B00101000}, //            ..#.#...
//---------------------------------------------
  {4, B00001000,  // 0x2B_+     ....#...   #11
      B00011100,  //            ...###..
      B00001000,  //            ....#...
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {2, B10110000,  // 0x2C_      #.##....   #12
      B01110000,  //            .###....
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {4, B00001000,  // 0x2D_-     ....#...   #13
      B00001000,  //            ....#...
      B00001000,  //            ....#...
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {3, B01100000,  // 0x2E_.     .##.....   #14
      B01100000,  //            .##.....
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {4, B01100000,  // 0x2F_/     .##.....   #15
      B00011000,  //            ...##...
      B00000110,  //            .....##.
      B00000001,  //            .......#
      B00000000}, //            ........
//---------------------------------------------
  {4, B00111110,  // 0x30_0     ..#####.   #16
      B01000001,  //            .#.....#
      B01000001,  //            .#.....#
      B00111110,  //            ..#####.
      B00000000}, //            ........
//---------------------------------------------
  {3, B01000010,  // 0x31_1     .#....#.   #17
      B01111111,  //            .#######
      B01000000,  //            .#......
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {4, B01100010,  // 0x32_2     .##...#.   #18
      B01010001,  //            .#.#...#
      B01001001,  //            .#..#..#
      B01000110,  //            .#...##.
      B00000000}, //            ........
//---------------------------------------------
  {4, B00100010,  // 0x33_3     ..#...#.   #19
      B01000001,  //            .#.....#
      B01001001,  //            .#..#..#
      B00110110,  //            ..##.##.
      B00000000}, //            ........
//---------------------------------------------
  {4, B00011000,  // 0x34_4     ...##...   #20
      B00010100,  //            ...#.#..
      B00010010,  //            ...#..#.
      B01111111,  //            .#######
      B00000000}, //            ........
//---------------------------------------------
  {4, B00100111,  // 0x35_5     ..#..###   #21
      B01000101,  //            .#...#.#
      B01000101,  //            .#...#.#
      B00111001,  //            ..###..#
      B00000000}, //            ........
//---------------------------------------------
  {4, B00111110,  // 0x36_6     ..#####.   #22
      B01001001,  //            .#..#..#
      B01001001,  //            .#..#..#
      B00110000,  //            ..##....
      B00000000}, //            ........
//---------------------------------------------
  {4, B01100001,  // 0x37_7     .##....#   #23
      B00010001,  //            ...#...#
      B00001001,  //            ....#..#
      B00000111,  //            .....###
      B00000000}, //            ........
//---------------------------------------------
  {4, B00110110,  // 0x38_8     ..##.##.   #24
      B01001001,  //            .#..#..#
      B01001001,  //            .#..#..#
      B00110110,  //            ..##.##.
      B00000000}, //            ........
//---------------------------------------------
  {4, B00000110,  // 0x39_9     .....##.   #25
      B01001001,  //            .#..#..#
      B01001001,  //            .#..#..#
      B00111110,  //            ..#####.
      B00000000}, //            ........
//---------------------------------------------
  {2, B01010000,  // 0x3A_:     .#.#....   #26
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {2, B10000000,  // 0x3B_;     #.......   #27
      B01010000,  //            .#.#....
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {3, B00010000,  // 0x3C_<     ...#....   #28
      B00101000,  //            ..#.#...
      B01000100,  //            .#...#..
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {3, B00010100,  // 0x3D_=     ...#.#..   #29
      B00010100,  //            ...#.#..
      B00010100,  //            ...#.#..
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {3, B01000100,  // 0x3E_>     .#...#..   #30
      B00101000,  //            ..#.#...
      B00010000,  //            ...#....
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {4, B00000010,  // 0x3F_?     ......#.   #31
      B01011001,  //            .#.##..#
      B00001001,  //            ....#..#
      B00000110,  //            .....##.
      B00000000}, //            ........
//---------------------------------------------
  {5, B00111110,  // 0x40_@     ..#####.   #32
      B01001001,  //            .#..#..#
      B01010101,  //            .#.#.#.#
      B01011101,  //            .#.###.#
      B00001110}, //            ....###.
//---------------------------------------------
  {4, B01111110,  // 0x41_A     .######.   #33
      B00010001,  //            ...#...#
      B00010001,  //            ...#...#
      B01111110,  //            .######.
      B00000000}, //            ........
//---------------------------------------------
  {4, B01111111,  // 0x42_B     .#######   #34
      B01001001,  //            .#..#..#
      B01001001,  //            .#..#..#
      B00110110,  //            ..##.##.
      B00000000}, //            ........
//---------------------------------------------
  {4, B00111110,  // 0x43_C     ..#####.   #35
      B01000001,  //            .#.....#
      B01000001,  //            .#.....#
      B00100010,  //            ..#...#.
      B00000000}, //            ........
//---------------------------------------------
  {4, B01111111,  // 0x44_D     .#######   #36
      B01000001,  //            .#.....#
      B01000001,  //            .#.....#
      B00111110,  //            ..#####.
      B00000000}, //            ........
//---------------------------------------------
  {4, B01111111,  // 0x45_E     .#######   #37
      B01001001,  //            .#..#..#
      B01001001,  //            .#..#..#
      B01000001,  //            .#.....#
      B00000000}, //            ........
//---------------------------------------------
  {4, B01111111,  // 0x46_F     .#######   #38
      B00001001,  //            ....#..#
      B00001001,  //            ....#..#
      B00000001,  //            .......#
      B00000000}, //            ........
//---------------------------------------------
  {4, B00111110,  // 0x47_G     ..#####.   #39
      B01000001,  //            .#.....#
      B01001001,  //            .#..#..#
      B01111010,  //            .####.#.
      B00000000}, //            ........
//---------------------------------------------
  {4, B01111111,  // 0x48_H     .#######   #40
      B00001000,  //            ....#...
      B00001000,  //            ....#...
      B01111111,  //            .#######
      B00000000}, //            ........
//---------------------------------------------
  {3, B01000001,  // 0x49_I     .#.....#   #41
      B01111111,  //            .#######
      B01000001,  //            .#.....#
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {4, B00110000,  // 0x4A_J     ..##....   #42
      B01000000,  //            .#......
      B01000001,  //            .#.....#
      B00111111,  //            ..######
      B00000000}, //            ........
//---------------------------------------------
  {4, B01111111,  // 0x4B_K     .#######   #43
      B00001000,  //            ....#...
      B00010100,  //            ...#.#..
      B01100011,  //            .##...##
      B00000000}, //            ........
//---------------------------------------------
  {4, B01111111,  // 0x4C_L     .#######   #44
      B01000000,  //            .#......
      B01000000,  //            .#......
      B01000000,  //            .#......
      B00000000}, //            ........
//---------------------------------------------
  {5, B01111111,  // 0x4D_M     .#######   #45
      B00000010,  //            ......#.
      B00001100,  //            ....##..
      B00000010,  //            ......#.
      B01111111}, //            .#######
//---------------------------------------------
  {5, B01111111,  // 0x4E_N     .#######   #46
      B00000100,  //            .....#..
      B00001000,  //            ....#...
      B00010000,  //            ...#....
      B01111111}, //            .#######
//---------------------------------------------
  {4, B00111110,  // 0x4F_O     ..#####.   #47
      B01000001,  //            .#.....#
      B01000001,  //            .#.....#
      B00111110,  //            ..#####.
      B00000000}, //            ........
//---------------------------------------------
  {4, B01111111,  // 0x50_P     .#######   #48
      B00001001,  //            ....#..#
      B00001001,  //            ....#..#
      B00000110,  //            .....##.
      B00000000}, //            ........
//---------------------------------------------
  {4, B00111110,  // 0x51_Q     ..#####.   #49
      B01000001,  //            .#.....#
      B01000001,  //            .#.....#
      B10111110,  //            #.#####.
      B00000000}, //            ........
//---------------------------------------------
  {4, B01111111,  // 0x52_R     .#######   #50
      B00001001,  //            ....#..#
      B00001001,  //            ....#..#
      B01110110,  //            .###.##.
      B00000000}, //            ........
//---------------------------------------------
  {4, B01000110,  // 0x53_S     .#...##.   #51
      B01001001,  //            .#..#..#
      B01001001,  //            .#..#..#
      B00110010,  //            ..##..#.
      B00000000}, //            ........
//---------------------------------------------
  {5, B00000001,  // 0x54_T     .......#   #52
      B00000001,  //            .......#
      B01111111,  //            .#######
      B00000001,  //            .......#
      B00000001}, //            .......#
//---------------------------------------------
  {4, B00111111,  // 0x55_U     ..######   #53
      B01000000,  //            .#......
      B01000000,  //            .#......
      B00111111,  //            ..######
      B00000000}, //            ........
//---------------------------------------------
  {5, B00001111,  // 0x56_V     ....####   #54
      B00110000,  //            ..##....
      B01000000,  //            .#......
      B00110000,  //            ..##....
      B00001111}, //            ....####
//---------------------------------------------
  {5, B00111111,  // 0x57_W     ..######   #55
      B01000000,  //            .#......
      B00111000,  //            ..###...
      B01000000,  //            .#......
      B00111111}, //            ..######
//---------------------------------------------
  {5, B01100011,  // 0x58_X     .##...##   #56
      B00010100,  //            ...#.#..
      B00001000,  //            ....#...
      B00010100,  //            ...#.#..
      B01100011}, //            .##...##
//---------------------------------------------
  {5, B00000111,  // 0x59_Y     .....###   #57
      B00001000,  //            ....#...
      B01110000,  //            .###....
      B00001000,  //            ....#...
      B00000111}, //            .....###
//---------------------------------------------
  {4, B01100001,  // 0x5A_Z     .##....#   #58
      B01010001,  //            .#.#...#
      B01001001,  //            .#..#..#
      B01000111,  //            .#...###
      B00000000}, //            ........
//---------------------------------------------
  {2, B01111111,  // 0x5B_[     .#######   #59
      B01000001,  //            .#.....#
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {4, B00000001,  // 0x5C_\     .......#   #60
      B00000110,  //            .....##.
      B00011000,  //            ...##...
      B01100000,  //            .##.....
      B00000000}, //            ........
//---------------------------------------------
  {2, B01000001,  // 0x5D_]     .#.....#   #61
      B01111111,  //            .#######
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {3, B00000010,  // 0x5E_hat   ......#.   #62
      B00000001,  //            .......#
      B00000010,  //            ......#.
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {4, B01000000,  // 0x5F__     .#......   #63
      B01000000,  //            .#......
      B01000000,  //            .#......
      B01000000,  //            .#......
      B00000000}, //            ........
//---------------------------------------------
  {2, B00000001,  // 0x60_`     .......#   #64
      B00000010,  //            ......#.
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {4, B00100000,  // 0x61_a     ..#.....   #65
      B01010100,  //            .#.#.#..
      B01010100,  //            .#.#.#..
      B01111000,  //            .####...
      B00000000}, //            ........
//---------------------------------------------
  {4, B01111111,  // 0x62_b     .#######   #66
      B01000100,  //            .#...#..
      B01000100,  //            .#...#..
      B00111000,  //            ..###...
      B00000000}, //            ........
//---------------------------------------------
  {3, B00111000,  // 0x63_c     ..###...   #67
      B01000100,  //            .#...#..
      B01000100,  //            .#...#..
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {4, B00111000,  // 0x64_d     ..###...   #68
      B01000100,  //            .#...#..
      B01000100,  //            .#...#..
      B01111111,  //            .#######
      B00000000}, //            ........
//---------------------------------------------
  {4, B00111000,  // 0x65_e     ..###...   #69
      B01010100,  //            .#.#.#..
      B01010100,  //            .#.#.#..
      B00011000,  //            ...##...
      B00000000}, //            ........
//---------------------------------------------
  {3, B00000100,  // 0x66_f     .....#..   #70
      B01111110,  //            .######.
      B00000101,  //            .....#.#
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {4, B10011000,  // 0x67_g     #..##...   #71
      B10100100,  //            #.#..#..
      B10100100,  //            #.#..#..
      B01111000,  //            .####...
      B00000000}, //            ........
//---------------------------------------------
  {4, B01111111,  // 0x68_h     .#######   #72
      B00000100,  //            .....#..
      B00000100,  //            .....#..
      B01111000,  //            .####...
      B00000000}, //            ........
//---------------------------------------------
  {3, B01000100,  // 0x69_i     .#...#..   #73
      B01111101,  //            .#####.#
      B01000000,  //            .#......
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {4, B01000000,  // 0x6A_j     .#......   #74
      B10000000,  //            #.......
      B10000100,  //            #....#..
      B01111101,  //            .#####.#
      B00000000}, //            ........
//---------------------------------------------
  {4, B01111111,  // 0x6B_k     .#######   #75
      B00010000,  //            ...#....
      B00101000,  //            ..#.#...
      B01000100,  //            .#...#..
      B00000000}, //            ........
//---------------------------------------------
  {3, B01000001,  // 0x6C_l     .#.....#   #76
      B01111111,  //            .#######
      B01000000,  //            .#......
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {5, B01111100,  // 0x6D_m     .#####..   #77
      B00000100,  //            .....#..
      B01111100,  //            .#####..
      B00000100,  //            .....#..
      B01111000}, //            .####...
//---------------------------------------------
  {4, B01111100,  // 0x6E_n     .#####..   #78
      B00000100,  //            .....#..
      B00000100,  //            .....#..
      B01111000,  //            .####...
      B00000000}, //            ........
//---------------------------------------------
  {4, B00111000,  // 0x6F_o     ..###...   #79
      B01000100,  //            .#...#..
      B01000100,  //            .#...#..
      B00111000,  //            ..###...
      B00000000}, //            ........
//---------------------------------------------
  {4, B11111100,  // 0x70_p     ######..   #80
      B00100100,  //            ..#..#..
      B00100100,  //            ..#..#..
      B00011000,  //            ...##...
      B00000000}, //            ........
//---------------------------------------------
  {4, B00011000,  // 0x71_q     ...##...   #81
      B00100100,  //            ..#..#..
      B00100100,  //            ..#..#..
      B11111100,  //            ######..
      B00000000}, //            ........
//---------------------------------------------
  {4, B01111100,  // 0x72_r     .#####..   #82
      B00001000,  //            ....#...
      B00000100,  //            .....#..
      B00000100,  //            .....#..
      B00000000}, //            ........
//---------------------------------------------
  {4, B01001000,  // 0x73_s     .#..#...   #83
      B01010100,  //            .#.#.#..
      B01010100,  //            .#.#.#..
      B00100100,  //            ..#..#..
      B00000000}, //            ........
//---------------------------------------------
  {3, B00000100,  // 0x74_t     .....#..   #84
      B00111111,  //            ..######
      B01000100,  //            .#...#..
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {4, B00111100,  // 0x75_u     ..####..   #85
      B01000000,  //            .#......
      B01000000,  //            .#......
      B01111100,  //            .#####..
      B00000000}, //            ........
//---------------------------------------------
  {3, B00111000,  // 0x76_v     ..###...   #86
      B01000000,  //            .#......
      B00111000,  //            ..###...
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {5, B00111100,  // 0x77_w     ..####..   #87
      B01000000,  //            .#......
      B00111100,  //            ..####..
      B01000000,  //            .#......
      B00111100}, //            ..####..
//---------------------------------------------
  {5, B01000100,  // 0x78_x     .#...#..   #88
      B00101000,  //            ..#.#...
      B00010000,  //            ...#....
      B00101000,  //            ..#.#...
      B01000100}, //            .#...#..
//---------------------------------------------
  {4, B10011100,  // 0x79_y     #..###..   #89
      B10100000,  //            #.#.....
      B10100000,  //            #.#.....
      B01111100,  //            .#####..
      B00000000}, //            ........
//---------------------------------------------
  {3, B01100100,  // 0x7A_z     .##..#..   #90
      B01010100,  //            .#.#.#..
      B01001100,  //            .#..##..
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {3, B00001000,  // 0x7B_      ....#...   #91
      B00110110,  //            ..##.##.
      B01000001,  //            .#.....#
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {1, B01111111,  // 0x7C_|     .#######   #92
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {3, B01000001,  // 0x7D_}     .#.....#   #93
      B00110110,  //            ..##.##.
      B00001000,  //            ....#...
      B00000000,  //            ........
      B00000000}, //            ........
//---------------------------------------------
  {4, B00001000,  // 0x7E_~     ....#...   #94
      B00000100,  //            .....#..
      B00001000,  //            ....#...
      B00000100,  //            .....#..
      B00000000}, //            ........
//---------------------------------------------
  {1, B00000000,  // 0x7F_stop  ########   #95
      B00000000,  //            ########
      B00000000,  //            ########
      B00000000,  //            ########
      B00000000}  //            ########
};

void setup()
{

  DispString[0]='\0';
  //  digitalWrite(pinLED, LOW);

//  int countdownMS = Watchdog.enable(4000);

  timerHourTick=5000;

//  BarToDisplay=0;

  swReedAntiBumpACT = RE_BUMPFILTER_ACT;        // Reset antibump di ACTIVE
  swReedAntiBumpNACT = RE_BUMPFILTER_NOTACT;    // Reset antibump di NOTACT

  flgReedStatus = false;
  flgReedPulse  = false;
  timReedOn  = 0;
  timReedOff = 0;
  timReedLastOn  = 0;
  timReedLastOff = 0;
  flgSUNBRun = false;
  Stop_smWindAlarm = false;     // Reset flag

  ResetRequest = 0;
  ResetRequestOnError = true;
  
  SUNBpos   =  0;
  SUNBstep  =  0;

  // put your setup code here, to run once:
  pinMode(pinOpenIn,  INPUT_PULLUP);
  pinMode(pinDownIn,  INPUT_PULLUP);
  pinMode(pinWindSwitch_int,
          INPUT_PULLUP);
  pinMode(pinOpenOut, OUTPUT);
  pinMode(pinDownOut, OUTPUT);
  pinMode(pinLED,     OUTPUT);      // sets the digital pin as output

  // 100mS loop per lettura switch
  Tmr.every(1L, IntTim_ReadSwitch);

  Serial.begin(115200);
#if MonitorMODE == 1

  Serial.print("- Controllo Tenda ");

#if TESTMODE == 1
  Serial.print  (VERSION_);
  Serial.println(" (TESTMODE) ");
#else
  Serial.println(VERSION_);
#endif

  Serial.println("- Commands d|D|s|u|U|w|m|H");
#endif
//  Watchdog.reset();

/*
   The MAX72XX is in power-saving mode on startup,
   we have to do a wakeup call
   */
//Serial.println("> shutdown");
  lc.shutdown(0,false);
  lc.shutdown(1,false);
  lc.shutdown(2,false);
  lc.shutdown(3,false);
//Serial.println("> bright");
  /* Set the brightness to a medium values */
  lc.setIntensity(0,5);
  lc.setIntensity(1,5);
  lc.setIntensity(2,5);
  lc.setIntensity(3,5);
//Serial.println("> clear");
  /* and clear the display */
  lc.clearDisplay(0);
  lc.clearDisplay(1);
  lc.clearDisplay(2);
  lc.clearDisplay(3);

//Serial.println("> open");

  char ver[40]=VERSION_;
  MatrixWriteString("Sun",      9,true); delay(500); PinSwitch(); delay(500); PinSwitch();
  MatrixWriteString("blind" ,   5,true); delay(500); PinSwitch(); delay(500); PinSwitch();
  MatrixWriteString("control",  0,true); delay(500); PinSwitch(); delay(500); PinSwitch();
  MatrixWriteString(ver+10,2,true);      delay(500); PinSwitch(); delay(500); PinSwitch();
#if MonitorMODE == 1
  Serial.println("- Tenda indietro");
#endif
  int showPin;
  digitalWrite(pinDownOut, HIGH); // showPin = digitalRead(pinDownOut);  Serial.print(showPin);
  digitalWrite(pinOpenOut, HIGH); // showPin = digitalRead(pinOpenOut);  Serial.println(showPin);
  delay(500); PinSwitch(); delay(500); PinSwitch();
  digitalWrite(pinDownOut, LOW);  // showPin = digitalRead(pinDownOut);  Serial.print(showPin);
  digitalWrite(pinOpenOut, HIGH); // showPin = digitalRead(pinOpenOut);  Serial.println(showPin);
  delay(500); PinSwitch(); delay(500); PinSwitch();
  digitalWrite(pinDownOut, HIGH); // showPin = digitalRead(pinDownOut);  Serial.print(showPin);
  digitalWrite(pinOpenOut, HIGH); // showPin = digitalRead(pinOpenOut);  Serial.println(showPin);

  for(uint8_t cnt=60; cnt>0; cnt--)
  {
    char str[8];
    delay(500); PinSwitch(); delay(500); PinSwitch();
    if (cnt%10==0)
    {
      Serial.print("- ");
      Serial.println(cnt);
    }
    MatrixWriteString("Wait:", 0,true);
    sprintf(str,"%d", cnt);
    MatrixWriteString(str,23,false);
    if (digitalRead(pinDownIn) == LOW) cnt = 1;
    if (digitalRead(pinOpenIn) == LOW) cnt = 1;
  }

  while(digitalRead(pinDownIn) == LOW);
  while(digitalRead(pinOpenIn) == LOW);

  Serial.println("- Fine");

  MatrixWriteString("Run! ! !",1,true);

  delay(500); PinSwitch(); delay(500); PinSwitch();

  attachInterrupt(digitalPinToInterrupt(pinWindSwitch_int), Int3_MeasureWind, FALLING);

  dht.begin();
//  arrayStart=arraySize;
  SUNBStart=true;
}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void loop()
{
  static bool DisplayChange=false; 
  static byte DisplaySel=1;
  static int  MediaNumber;

  // put your main code here, to run repeatedly:


// MatrixWriteString("blind",0,true); delay(1000);
// MatrixWriteString("ctrl" ,0,true); delay(1000);

  Tmr.update();
#if PlotterMODE == 1
  if (SendPlot && enPlotterMode) {
    Serial.print(SUNBpos);
    Serial.print(" ");
    Serial.print(WindSpeed);
    Serial.print(" ");
    Serial.print(timReedLastOff/100);
    Serial.print(" ");
    Serial.print(timReedLastOn/100);
    Serial.print(" ");
    Serial.print(WINDALARM);
    Serial.println(" ");
    SendPlot=false;
  }
#endif
#if MonitorMODE == 1
  if (SendMsg && enMonitorMode) {
    Serial.print("-");
//  Serial.print(" T:");      Serial.print(Temperature);
//  Serial.print(" H:");      Serial.print(Humidity);
    Serial.print(" OpenIn:"); Serial.print(digitalRead(pinOpenIn));
    Serial.print(" DownIn:"); Serial.print(digitalRead(pinDownIn));
    Serial.print(" OpenOu:"); Serial.print(pinOpenOutHold); pinOpenOutHold = 1;
    Serial.print(" DownOu:"); Serial.print(pinDownOutHold); pinDownOutHold = 1;
//  Serial.print(" CntOpn:"); Serial.print(CntSwOpen);
//  Serial.print(" CntDow:"); Serial.print(CntSwDown);
//  Serial.print(" CntWdt:"); Serial.print(CntWdt);
    Serial.print(" TenSte:"); Serial.print(SUNBstep);
    Serial.print(" TenPos:"); Serial.print(SUNBpos);
//  Serial.print(" RstReq:"); Serial.print(ResetRequest);
//  Serial.print(" TenPoH:"); Serial.print(SUNBposHold/10);
//  Serial.print(" smSUNB:"); Serial.print(smSUNBCommand);
//  Serial.print(" fPulse:"); Serial.print(flgReedPulse);
//  Serial.print(" OpenIn:"); Serial.print(digitalRead(pinPosReed));
//  Serial.print(" timROf:"); Serial.print(timReedOff);
//  Serial.print(" timROn:"); Serial.print(timReedOn);
//  Serial.print(" swReed:"); Serial.print(flgReedStatus);
//  Serial.print(" timRof:"); Serial.print(timReedLastOff);
//  Serial.print(" timRon:"); Serial.print(timReedLastOn);
//  Serial.print(" timRee:"); Serial.print(timReedOn+timReedOff);
    Serial.print(" WndSpd:"); Serial.print(WindSpeed);
    Serial.print(" smWind:"); Serial.print(smWindAlarm);
//   Serial.print(" timWnd:"); Serial.print(timWind);
//  Serial.print(" tmHour:"); Serial.print(timerHourTick);
//  Serial.print(" tmMin:");  Serial.print(timerMinutes);
//  Serial.print(" tmSec:");  Serial.print(timerSeconds);
    Serial.print(" staLED:"); Serial.print(StatusLED);
//  Serial.print(" inChar:"); Serial.print(inChar);
//  Serial.print(" mTemp:");  Serial.print(meanTemperature);
//  Serial.print(" mHumid:"); Serial.print(meanHumidity);
//  Serial.print(" mPosit:"); Serial.print(meanSUNBpos);
//  Serial.print(" mWind:");  Serial.print(meanWindSpeed);

    Serial.print(" Disp: ");  Serial.print(DispString);

    Serial.println(".");
    SendMsg = false;
//    inChar='0';
//DGB// Serial.print(": RoutineInLoop1sec ");Serial.println(RoutineInLoop1sec); // ----------------- DEBUG
//DGB// Serial.print(": arrayStart "); Serial.println(arrayStart); // ----------------- DEBUG
//DG// Serial.print(": SUNBStart ");Serial.println(SUNBStart); // ----------------- DEBUG
  }
#endif

  if (RoutineInLoop1sec) // && (arrayStart==0) && (SUNBStart==false))
  {
    if(timeOutOverheat == 0) MatrixWriteString("Errore!", 0,true); // Motor error
    else
    {
      if(DisplayChange)
      {
        switch (DisplaySel)                                         // Display switcher
        {                                                           //
          int intTemp;                                              //
          case (DISPLAYTEMP): //---- Temperature -----------        // Display temperatura
          char str[10];                                             //
          MatrixWriteString("t", 0,true);                           // Stampa 't'
                                                                    //
          intTemp=(int)(Temperature*10);                            // 
          if((intTemp%10)>5) intTemp=intTemp/10+1;                  // Temperatura in formato integer
          else               intTemp=intTemp/10;                    //
                                                                    //
          sprintf(str,"%d",abs(intTemp));                           // Stampa valore temperatura
          if (Temperature<0)  { MatrixWriteString("-",4,false);     // Stampa '-' se negativa
                                MatrixWriteString(str,7,false);}    // Stampa valore di temp negativa
          else                  MatrixWriteString(str,4,false);     // Stampa valore di temp positiva 
    //DBG Serial.print("T "); // ----------------- DEBUG            //
          MatrixWriteBar(arrayTemp, 16, BARTEMP);                   // Stampa barra temperatura
          break;                                                    //
                                                                    //
          case (DISPLAYHUMID): //---- Humidity --------------       //
          MatrixWriteString("h",0,true);                            //
          sprintf(str,"%d",(int)Humidity);                          //
          MatrixWriteString(str,5,false);                           //
    //DBG Serial.print("H "); // ----------------- DEBUG            //
          MatrixWriteBar(arrayHumid,16, BARHUMID);                  //
          break;                                                    //
                                                                    //
          case (DISPLAYPOS): //---- Sun Blind position              //
          MatrixWriteString("p", 0,true);                           //
          sprintf(str,"%d",SUNBpos);                                //
          MatrixWriteString(str,5,false);                           //
    //DBG Serial.print("P "); // ----------------- DEBUG            //
          if      (SUNBstep>12)                                     //
              MatrixWriteString("<<<<<",15,false);                  //
          else if (SUNBstep>6 )                                     //
              MatrixWriteString("<<<<", 15,false);                  //
          else if (SUNBstep>4 )                                     //
              MatrixWriteString("<<<",  15,false);                  //
           else if (SUNBstep>0)                                     //
              MatrixWriteString("<<",   15,false);                  //
          else if (SUNBstep<-12)                                    //
              MatrixWriteString(">>>>>",15,false);                  //
          else if (SUNBstep<-6)                                     //
              MatrixWriteString(">>>>", 15,false);                  //
          else if (SUNBstep<-4)                                     //
              MatrixWriteString(">>>",  15,false);                  //
          else if (SUNBstep<0)                                      //
              MatrixWriteString(">>",   15,false);                  //
          else                                                      //
            MatrixWriteBar(arrayPosit,16, BARPOSIT);                //
          break;                                                    //
                                                                    //
          case (DISPLAYWSPEED): //---- Wind Speed                   //
          MatrixWriteString("w",0,true);                            //
          sprintf(str,"%d",WindSpeed);                              //
          MatrixWriteString(str,6,false);                           //
    //DBG Serial.print("W "); // ----------------- DEBUG            //
                                                                    //
          switch(smWindAlarm)                                       //
          {                                                         //
            case WA_NOALARM_00:                                     //
              MatrixWriteBar(arrayWind,16, BARWIND);                //
              break;                                                //
            case WA_ALARM_10:                                       //
            case WA_MOVETOZERO_01:                                  //
            case WA_MOVETO1STSTEP_02:                               //
              MatrixWriteString(">>>",16,false);                    //
              break;                                                //
            case WA_WAITENDALARM_03:                                //
              sprintf(str,"-%d",timWind);                           //
              MatrixWriteString(str,16,false);                      //
              break;                                                //
            case WA_WAITENDRESTORE_04:                              //
              MatrixWriteString("<<<",16,false);                    //
              break;                                                //
            default:                                                //
              break;                                                //
          }                                                         //
          break;                                                    //
          default:
//        case (DISPLAYREALTIM):
            MatrixWriteString("Rt",0,true);
            if(timerWeeks>=99)
            {
              sprintf(str,"%d", "99+w");
            }
            else if(timerWeeks>0)
            {
              sprintf(str,"%d", timerWeeks);
              strcat(str,"w");          
            }
            else if(timerDays>0)
            {
              sprintf(str,"%d", timerDays);
              strcat(str,"d");          
            }
            else if(timerHours>0)
            {
              sprintf(str,"%d", timerHours);
              strcat(str,"h");          
            }
            else if(timerMinutes>0)
            {
              sprintf(str,"%d", timerMinutes);
              strcat(str,"m");          
            }
//          if(strlen(str)>4) str[4]='\0';
            MatrixWriteString(str,10,false);
//          break;
//        default:
            DisplaySel=0;                                           //
            break;                                                  //
        }                                                           //
                                                                    //
        CleanBarDisplay();                                          //
                                                                    //
        if (smWindAlarm!=WA_NOALARM_00) DisplaySel=DISPLAYWSPEED;   //
        else if (SUNBstep!=0)           DisplaySel=DISPLAYPOS;      //
        else                                                        //
        {                                                           //
          ++DisplaySel;                                             //
          DisplayChange=false;                                      //
        }                                                           //
      }
      else
        DisplayChange=true;
        
  // Qui nuove routines
  //    if(flgReadTemp==false) flgReadTemp=true; else {flgReadTemp=false; ReadTempHigro();};
      if ((flgSUNBRun==false) && (smWindAlarm==WA_NOALARM_00)) ReadTempHigro();
      if (timerWindDelta < 36000) timerWindDelta++;

      if (inChar =='H') {timerHourTick=3595; inChar='0';}

      if (++timerSeconds>=60) 
      {
        timerSeconds = 0;
        if(++timerMinutes>=60)
        {
          timerMinutes = 0;
          if (++timerHours>=24) 
          {
            timerHours=0;
            if(++timerDays>=7)
            {
              timerDays='0';
              if(timerWeeks<99)
              {
                timerWeeks++;
              }
            }
          }
        }
      };
      
      if (tickHour)
      {
        MediaNumber = 1;

        arrayHumid[0]= (int)meanHumidity;
        arrayTemp[0] = (int)(meanTemperature);
        arrayPosit[0]= meanSUNBpos;
        arrayWind[0] = meanWindSpeed;

        for(uint8_t p=14;p>0;p--)
        {
          arrayHumid[p]=arrayHumid[p-1];
          arrayPosit[p]=arrayPosit[p-1];
          arrayTemp[p] =arrayTemp[p-1];
          arrayWind[p] =arrayWind[p-1];

          meanHumidity    = Humidity;
          meanSUNBpos     = 0;
          meanTemperature = Temperature;
          meanWindSpeed   = 0;
        }
        tickHour = false;
      }
      else
      {
        if(MediaNumber==1)
        {
          meanHumidity=(Humidity+meanHumidity)/2;
          meanTemperature=(Temperature+meanTemperature)/2;
        }
        else
        {
          meanHumidity   =((meanHumidity   *MediaNumber)+Humidity)   /(MediaNumber+1);
          meanTemperature=((meanTemperature*MediaNumber)+Temperature)/(MediaNumber+1);
        }
        MediaNumber++;
        if (meanSUNBpos<SUNBpos)     meanSUNBpos = SUNBpos;
        if (meanWindSpeed<WindSpeed) meanWindSpeed = WindSpeed;
      }

      arrayHumid[0]=(int)Humidity;
      arrayTemp[0] =(int)Temperature;
      arrayPosit[0]=SUNBpos;
      arrayWind[0] =meanWindSpeed;

      RoutineInLoop1sec=false;
    }
  }
//  valid inChar d|D|s|u|U|w|m|p

#if MonitorMODE == 1
  if (inChar=='0') inChar = Serial.read();
  if (inChar==-1)  inChar='0';
  if (inChar=='m')
    {
      if (enMonitorMode == true) enMonitorMode = false;
      else                       enMonitorMode = true;
      inChar='0';
      Serial.print("+ Monitor Mode: ");
      Serial.println(enMonitorMode);
    }
  if (inChar=='p')
    {
      if (enPlotterMode == true) enPlotterMode = false;
      else                       enPlotterMode = true;
      inChar='0';
      Serial.print("+ Plotter Mode: ");
      Serial.println(enPlotterMode);
    }

#endif

}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void Int3_MeasureWind()
{
  swWindAntiBump = WA_BUMPFILTER; // Interrupt anemometro attiva il test dei pin con antibup
}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void IntTim_ReadSwitch() // Routine di servizio timer
{
  static bool flgReadTemp;

// ## Routine 1/1000 di secondo -------------------------------------------------------------------
// -- Incremento contatore vento con antibump -----------------------------------------------------
  if (swWindAntiBump  > 1)                                         // Switch antibump non scaduto
  {
    if (digitalRead(pinWindSwitch_int) == LOW) swWindAntiBump--;   // Se switch wind attivo decrementa contatore antibump
    else                                       swWindAntiBump = 0; // Disattiva contatore
  }
  if (swWindAntiBump == 1)                                         // Switch antibump scaduto
  {
    if (digitalRead(pinWindSwitch_int) == LOW) 
      {if (cntWind<0xFF) cntWind++;};                              // Se switch wind attivo Incrementa contatore vento...
    swWindAntiBump = 0;                                            // ...e disattiva antibump
  }
// -- Contatore giri (reed) -----------------------------------------------------------------------
  if (digitalRead(pinPosReed) == PIN_NOTACT)      // Reed non attivo
  {
    swReedAntiBumpACT = RE_BUMPFILTER_ACT;        // Reset antibump di ACTIVE
    if (swReedAntiBumpNACT > 0)                   // Contatore antibump NOTACT non scaduto
    {
      swReedAntiBumpNACT--;                       // Decrementa contatore NOTACT antibump
    }
    else
    {
      if (flgReedStatus)                          // Se Reed era attivo (TRUE)
      {
        flgReedPulse         = true;              // Impulso di giro
        flgReedPrintOnPulse  = true;              // Impulso di giro
        timReedLastOn = timReedOn;                // Salva tempo HIGH
      }
      flgReedStatus = false;                      // Set Reed non attivo
      timReedOn = 0;                              // Contatore HIGH azzerato
      if (flgSUNBRun) timReedOff++;                 // Se motore attivo incrementa contatore LOW
      else            timReedOff = 0;               // Altrimenti azzera
    }
  }
  else if (digitalRead(pinPosReed) == PIN_ACTIVE) // Reed attivo
  {
    swReedAntiBumpNACT = RE_BUMPFILTER_NOTACT;    // Reset antibump di NOTACT
    if (swReedAntiBumpACT > 0)                    // Contatore antibump ACTIVE non scaduto
    {
      swReedAntiBumpACT--;                        // Decrementa contatore ACTIVE antibump
    }
    else
    {
      if (!flgReedStatus)                         // Se Reed non era era attivo (FALSE)
      {
        flgReedPulse = false;                     // Cancella flag pulse (per sicurezza)
        timReedLastOff = timReedOff;              // Salva tempo LOW
      }
      flgReedStatus = true;                       // Set Reed HIGH
      timReedOff = 0;                             // Contatore LOW azzerato
      if (flgSUNBRun) timReedOn++;                // Se motore attivo incrementa contatore HIGH
      else            timReedOn = 0;              // Altrimenti azzera
    }
  }
  // ## Routine 1/20 di secondo --------------------------------------------------------------------
  if ((--cntRoutine50ms) == 0)
  {
    cntRoutine50ms = 50;
    // -- Incremento contatori switch -----------------------------------------------------------------
    if (digitalRead(pinOpenIn) == HIGH)
    {
      if(inChar!='0')
      {
        if      (inChar=='u' || inChar=='s') { CntSwOpen = SWITCHTIMEFILTER+1; inChar='0';}
        else if (inChar=='U')                { CntSwOpen = SWITCHTIMELONG+1;   inChar='0';}
      }
      else
      {
        CntSwOpen = 0; CntWdt = 0;
      }
    }
    else if (CntSwOpen < 0xFF) { CntSwOpen++;}

    if (digitalRead(pinDownIn) == HIGH)
    {
      if(inChar!='0')
      {
        if      (inChar=='d' || inChar=='s') { CntSwDown = SWITCHTIMEFILTER+1; inChar='0';}
        else if (inChar=='D')                { CntSwDown = SWITCHTIMELONG+1;   inChar='0';}
      }
      else
      {
        CntSwDown = 0; CntWdt = 0;
      }
    }
    else if (CntSwDown < 0xFF) { CntSwDown++; CntWdt++;}

    if ((digitalRead(pinOpenIn) == LOW) &&
        (digitalRead(pinDownIn) == LOW) ){ if (ResetRequest < 0xFFFF) ResetRequest++;} else ResetRequest = 0;

    // ------------------------------------------------------------------------------------------------
    // -- Movimento tenda -----------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------
    // -- SUNBCommandCtrl -----------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------
     switch (smSUNBCommand)                     // State machine SUNB Command
    {                                          //
      case CO_STARTUP_00:                      // Startup
        digitalWrite(pinDownOut, HIGH);        // Azzera comando Close
        digitalWrite(pinOpenOut, HIGH);        // Azzera comando Open
        smSUNBCommand = CO_WAIT_02;            // Next
        break;                                 //
      //---------------------------------------------------------------------------------------------
      case CO_STARTUP_01:                      // Spare state
        break;                                 //
      //---------------------------------------------------------------------------------------------
      case CO_WAIT_02:                                      // Idle, nulla attivo
        if (SUNBstep > 0) smSUNBCommand = CO_SUNBOPENON_03; // Se SUNB step > 0 OPEN
        if (SUNBstep < 0) smSUNBCommand = CO_SUNBDOWNON_04; // Se SUNB step < 0 CLOSE
        flgSUNBRun = false;                                 // Motore non attivo
        timeOutOverheat = TIMEROVERHEAT; // 10 minuti
        break;                                              //
      //---------------------------------------------------------------------------------------------
      case CO_SUNBOPENON_03:                   // Apri tenda, comandi OPEN=0 DOWN=1 (0=ON)
        digitalWrite(pinDownOut, HIGH);        // Disable close
        digitalWrite(pinOpenOut, LOW);         // Enable open
        flgSUNBRun = false;                    // Motore non attivo
        smSUNBCommand = CO_SUNBOPENACT_05;     // Next
        break;                                 //
      //---------------------------------------------------------------------------------------------
      case CO_SUNBDOWNON_04:                   // Chiudi tenda, comandi OPEN=1 DOWN=0 (0=ON)
        digitalWrite(pinDownOut, LOW);         // Enable close
        digitalWrite(pinOpenOut, HIGH);        // Disable open
        flgSUNBRun = false;                    // Motore non attivo
        smSUNBCommand = CO_SUNBDOWNACT_06;  // Next
        break;                                 //
      //---------------------------------------------------------------------------------------------
      case CO_SUNBOPENACT_05:               // Resetta comando open, comandi OPEN=1 DOWN=1 (0=ON)
        digitalWrite(pinDownOut, HIGH);        // Disable close
        digitalWrite(pinOpenOut, HIGH);        // Disable open
        flgSUNBRun = true;                     // Motore attivo
        if (flgReedPulse)                      // IMpulso da reed posizione
        {                                      //
          SUNBpos++;                           // Incrementa contatore posizione
          if (SUNBpos >= SUNBMAX)              // Se limite superiore...
          {
            SUNBstep = 0;                      // ...azzera contatore step,
            SUNBpos = SUNBMAX;
          }
          else                                 // altrimenti...
            SUNBstep--;                        // ...decrementa contatore step
          if (SUNBstep<=0)                     // Controllo se contatore step = 0 (o minore)
          {                                    //
            smSUNBCommand = CO_STOPMOTOR_07;   // Ferma motori
            SUNBstep = 0;                      // Azzera contatore step
          }                                    //
          flgReedPulse = false;                // Azzera flag reed posizione
        }
  
        if ((timReedOn+timReedOff)>RE_TIMEOUT) //
        {                                      //
          --timeOutOverheat;
          if (timeOutOverheat<=0)
            smSUNBCommand = CO_OVERHEATRESET_09;   // Ferma motori
          else if (timeOutOverheat%40==0)
          {
            smSUNBCommand = CO_SUNBOPENON_03;  // Torna indietro
          }
        }                                      //
        else
          timeOutOverheat = TIMEROVERHEAT;          // 10 minuti
                                               //
        break;                                 //
      //---------------------
      case CO_SUNBDOWNACT_06:                  // Resetta comando down, comandi OPEN=1 DOWN=1 (0=ON)
        digitalWrite(pinDownOut, HIGH);        // Disable close
        digitalWrite(pinOpenOut, HIGH);        // Disable open
        flgSUNBRun = true;                     // Motore attivo
        if (flgReedPulse)                      // IMpulso da reed posizione
        {                                      //
          SUNBpos--;                           // Decrementa contatore posizione
          if (SUNBpos <= SUNBMIN)              // Se limite inferiore...
          {
            SUNBstep = 0;                      // ...azzera contatore step,
            SUNBpos = SUNBMIN;
          }
          else                                 // altrimenti...
            SUNBstep++;                        // ...incrementa contatore step.
          flgReedPulse = false;                // Azzera flag reed posizione
          if (SUNBstep >= 0)                   // Controllo se contatore step = 0 (o maggiore)
          {                                    //
            smSUNBCommand = CO_STOPMOTOR_07;   // Ferma motori
            SUNBstep = 0;                      // Azzera contatore step
          }                                    //
        }
                                               // Timeout, tenda in fondo
        if ((timReedOn+timReedOff)>RE_TIMEOUT) //
        {                                      //
          --timeOutOverheat;
          if (timeOutOverheat<=0)
            smSUNBCommand = CO_OVERHEATRESET_09;   // Ferma motori
          else if (timeOutOverheat%40==0)
          {
            smSUNBCommand = CO_SUNBDOWNON_04;  // Torna indietro
          }
        }                                      //
        else
          timeOutOverheat = TIMEROVERHEAT;     // 10 minuti
        break;                                 //
      //---------------------
      case CO_STOPMOTOR_07: //  Blocca tenda, comandi OPEN=0 DOWN=0 (0=ON)
        digitalWrite(pinDownOut, LOW);
        digitalWrite(pinOpenOut, LOW);
        flgSUNBRun = false;
        smSUNBCommand = CO_CLEARCOMMAND_08;
        break;
      //---------------------
      case CO_CLEARCOMMAND_08: // Resetta, comandi OPEN=1 DOWN=1 (0=ON)
        digitalWrite(pinDownOut, HIGH);
        digitalWrite(pinOpenOut, HIGH);
        flgSUNBRun = false;
        smSUNBCommand = CO_WAIT_02;
        break;
      //---------------------
      case CO_OVERHEATRESET_09: //  Blocca tenda, comandi OPEN=0 DOWN=0 (0=ON)
        digitalWrite(pinDownOut, LOW);
        digitalWrite(pinOpenOut, LOW);
        flgSUNBRun = false;
        ResetRequestOnError = true;
        break;
    }
    if (digitalRead(pinOpenOut) == LOW) pinOpenOutHold = 0;
    if (digitalRead(pinDownOut) == LOW) pinDownOutHold = 0;
    // ------------------------------------------------------------------------------------------------
    // -- SUNBSwitchElab -----------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------
    if (SUNBpos>=17) SUNBpos = 17;
    if (SUNBpos<0)   SUNBpos = 0;
    
    if (CntSwOpen > SWITCHTIMEFILTER &&              // Stop SUNB, entrambii pulsanti attivi
        CntSwDown > SWITCHTIMEFILTER)                //
    {                                                //
      if (smWindAlarm == WA_NOALARM_00)              // Se non attivo allarme vento
      {                                              //
        CntSwOpen = 0; CntSwDown = 0;                // Azzera contatori
        SUNBstep =  0;                               // Stop tenda UP
      }                                              //
  //  ResetRequest++;
    }                                                //
    else if (CntSwOpen > SWITCHTIMEFILTER)           // Se open tenda
    {                                                //
      if (smWindAlarm == WA_NOALARM_00)              // Se non attivo allarme vento
      {                                              //
//    Serial.print("+ SUNBstep up find:"); Serial.println(SUNBstep);
        if (CntSwOpen > SWITCHTIMELONG)              // Se tempo lungo
        {
          if(SUNBpos<16) SUNBstep=SUNBOVER;          // LONG up
        }
        else if (SUNBstep==0)                        // Solo se non attivo SUNBstep
        {                                            //
          if(SUNBpos<16) SUNBstep=4-(SUNBpos%4);     // STEP up
//        Serial.print("+ SUNBstep up set:"); Serial.println(SUNBstep);
        }                                            //
      }                                              //
      else                                           //
      {                                              //
        CntSwOpen = 0;                               // Azzera contatore con vento
      }                                              //
    }                                                //
    else if (CntSwDown > SWITCHTIMEFILTER)           // Se down tenda
    {                                                //
      if (smWindAlarm == WA_NOALARM_00)              // Se non attivo allarme vento
      {                                              //
//    Serial.print("+ SUNBstep dw find:"); Serial.println(SUNBstep);
        if (CntSwDown > SWITCHTIMELONG)              // Se tempo lungo
        {
          if(SUNBpos>0) SUNBstep=-SUNBOVER;          // LONG dw  
        }
        else if (SUNBstep==0)                        // Solo se non attivo SUNBstep
        {                                            //
          if     (SUNBpos<=0) SUNBstep = 0;          // STEP dw
          else if(SUNBpos>=13)SUNBstep = 12-SUNBpos; //
          else                SUNBstep = 0-(((SUNBpos-1)%4)+1);
//        Serial.print("+ SUNBstep dw set:"); Serial.println(SUNBstep);
        }                                            //
      }                                              //
      else                                           //
      {                                              //
        Stop_smWindAlarm = true;                     // Ferma allarme vento, con vento
      }                                              //
    }
    // ------------------------------------------------------------------------------------------------
  }

  if ((--cntRoutine1000ms) <= 0)
  {
    cntRoutine1000ms = 1000;

//  if((ResetRequest<SWITCHTIMERESET) && ResetRequestOnError == false) { PinSwitch(); Serial.print("$");}
    PinSwitch(); Serial.print("$");
    
    // -- Display enabler -----------------------------------------------------------------------------

    // -- Message enabler -----------------------------------------------------------------------------

//    if (smWindAlarm != 0)   SendMsg = true;  // ### Flag PULSE
//    if (smSUNBCommand != 2) SendMsg = true;  // ### Flag PULSE
//    if (flgReedPrintOnPulse == true)  SendMsg = true;  // ### Flag PULSE
//    if (flgReedPrintOnPulse == true)  SendPlot = true;  // ### Flag PULSE
//    if (SUNBstep != 0)      SendMsg = true;  // ### Flag PULSE
//    if (SUNBpos   < 0)      SendMsg = true;  // ### Flag PULSE
//    if (WindSpeed > 0)      SendMsg = true;  // ### Flag PULSE
                              SendMsg = true;  // ### Flag PULSE
                              SendPlot = true;  // ### Flag PULSE
      flgReedPrintOnPulse = false;

    // -- Aggornamento wind speed ---------------------------------------------------------------------

    if(inChar=='w')
    {
      WindSpeed = 20; cntWind = 0; inChar='0';
    }
    else
    {
      WindSpeed = cntWind; cntWind = 0;
    }

//    inChar='0';

// ----------------------------------------------------------------------------
// -- WindControl -------------------------------------------------------------
// ----------------------------------------------------------------------------
    if (SUNBpos>=17) SUNBpos = 17;
    if (SUNBpos<0)   SUNBpos = 0;
  
    switch (smWindAlarm)
    {
      case WA_NOALARM_00:                           // State 00
        if (WindSpeed > WINDALARM &&              // Se wind speed maggiore di max safe...
            SUNBpos   > SUNBWINDPOS)                // ...e posizione maggiore di max safe
            if      (SUNBstep == 0)                 // Se tenda non in movimento
              smWindAlarm = WA_ALARM_10;            // Attiva routine chiusura per vento
            else if (SUNBstep > 0)                  // Se la tenda si sta aprendo
              SUNBstep=0;                           // Bloccala
        break;                                      // Se si sta chiudendo non fare nulla
      //---------------------$$$
      case WA_ALARM_10:                             // State 10
        if(SUNBpos<=SUNBWINDPOS) SUNBstep=0;                  // Setup movimento tenda con vento forte
        else           SUNBstep=SUNBWINDPOS-SUNBpos;          // WINDALARM down
                                                    //
        if (SUNBpos < SUNBMAXHOLD)                  //
          SUNBposHold = SUNBpos;                    //
        else                                        //
          SUNBposHold = SUNBMAXHOLD;                //
                                                    //
        smWindAlarm = WA_MOVETOZERO_01;             //
        break;                                      //
      //---------------------
      case WA_MOVETOZERO_01:                        // State 1
        if (SUNBpos == SUNBWINDPOS)                 // Attendi che tenda si metta in posizione safe
        {                                           //
          smWindAlarm = WA_MOVETO1STSTEP_02;        //
        }                                           //
        break;                                      //
      //---------------------
      case WA_MOVETO1STSTEP_02:                     // State 2
        timerWindDelta = 0;                         // Set timer
        timWind = WINDTIMER;                        //
        smWindAlarm = WA_WAITENDALARM_03;           //
        break;                                      //
      //---------------------
      case WA_WAITENDALARM_03:                      // State 3
        if (WindSpeed > WINDALARM)                  // Se allarme vento azzera il timer
        {                                           //
          timWind = WINDTIMER;                      //
        }                                           //
        else if (Stop_smWindAlarm)                  // Se premuto pulsante DOWN
        {
          Stop_smWindAlarm = false;                 // Cancella flag
          smWindAlarm = WA_NOALARM_00;              // Esci da smWindAlarm
        }
        else if (--timWind == 0)                    // Decrementa il timer
        {                                           //
          smWindAlarm = WA_WAITENDRESTORE_04;       // Ritorno in posizione
          SUNBstep = SUNBposHold - SUNBpos;               // WINDALARM up
        }                                           //
        break;                                      //
      //---------------------
      case WA_WAITENDRESTORE_04:                    // State 4
        smWindAlarm = WA_NOALARM_00;                // Ritorna dall'inizio
        break;                                      //
      //---------------------
      default:
        smWindAlarm = WA_NOALARM_00;
        break;
    }
//----
// ----------------------------------------------------------------------------
    RoutineInLoop1sec = true;

    if (SUNBpos<=0) SUNBStart=false;

//--- Spostatate routine in loop ----------------------------------------------
    if (timerHourTick<3600)
    {
      timerHourTick++;
    }
    else
    {
      timerHourTick = DISP_INTERVAL;
      tickHour = true;
    }
  }
}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ## Estrazione valori temperatura e umidità

void ReadTempHigro(void)
{
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  Humidity = dht.readHumidity();
  if (isnan(Humidity)) Humidity=40.0;
  // Read temperature as Celsius (the default)
  Temperature = dht.readTemperature();
  // Read temperature as Celsius (the default)
  if (isnan(Temperature)) Temperature=20.0;
   return;
}

// ## Elaborazione dello stato degli switch per attivare comandi tenda ----------


void PinSwitch()
{
  if (StatusLED)
  {
    digitalWrite(pinLED, HIGH);  StatusLED = false;
  }
  else
  {
    digitalWrite(pinLED, LOW); StatusLED = true;
  }
}

// ## Macchina a stati, controllo ed elaborazione dei dati dell'anemometro ------------------------



// ## Controllo comandi tenda ---------------------------------------------------------------------


// ## Funzioni display ----------------------------------------------------------------------------

//int FloatDivision(int dividend, int divisor)
//{
//  float fdividend, fdivisor, fquotient;
//  fdividend=(float)dividend;
//  fdivisor= (float)divisor;
//
//  fquotient=fdividend/fdivisor;
//
//  if (fquotient >= 0)
//    return((int)(fquotient + 0.5));
//  else
//    return((int)(fquotient - 0.5));
//}

void MatrixWriteBar(int8_t data[], int pos, char barset)
{
  float last=(float)data[0];
  float mini=last;
  float maxi=last;
  float ref=0, ndiv=1.0, actual;
  // --- CERCA il massimo e il minimo nell'array dati
  for(uint8_t p=1; p<arraySize; p++)
  {
      actual=(float)data[p];
      if (actual<mini) mini=actual;
      if (actual>maxi) maxi=actual;
//DBGif (barset==BARTEMP) {Serial.print(" data:");Serial.print(actual);} // ----------------- DEBUG
  }
//DBGif (barset==BARTEMP) Serial.println(" <<<<");// ----------------- DEBUG
  // --- Selezione BAR
  switch (barset)
  {
    case BARTEMP:
    case BARHUMID:
      if ((maxi-mini)>7)
      {
        ndiv = (maxi-mini)/8;
        ref  = mini/ndiv;
      }
      else
      {
        ndiv = 1;
        ref  = mini-3;
      }
      break;
    case BARPOSIT:
    case BARWIND:
      if (maxi>8) ndiv = maxi/8.0; else ndiv=1.0;
      ref  = -1.0;
      break;
    default:
      break;
  }

    for(uint8_t p=0;p<arraySize;p++) //------------------------ Display matrix pointer loop
    {
      byte val;
      val=(byte)(((float)data[p]/ndiv)-ref); //---------------- Val difference
      byte bar=0; //------------------------------------------- Bar filling
      if      (val<=0) {bar=0x01;} //-------------------------- All off bar
      else if (val> 7) {bar=0xFF;} //-------------------------- All on bar
      else //-------------------------------------------------- Bar filling and Matrix update
      {
        do { bar=(bar<<1)+1; val-=1;} while (val>0);
      }
      // LSB bar ON
      bar=bar|1;
      // ON OFF bit
      for(uint8_t row=0; row<=7; row++)
      {
        uint32_t MatrixAreaMask=1L<<(p);
        byte     barMask=1<<row;
        if ((bar & barMask)==0)   MatrixArea[7-row]&=~MatrixAreaMask; // OFF
        else                      MatrixArea[7-row]|= MatrixAreaMask; // ON
      }
    }
    // Barra verticale
    for(uint8_t row=0; row<=7; row++)
    {
      uint32_t MatrixAreaMask=1L<<arraySize;
      MatrixArea[7-row]|= MatrixAreaMask;
    }

};

int MatrixWriteChar(char c, int posChar, int rowChar)
{
  byte      RotCH[9];
  uint32_t  maskMatrix;
  byte      maskChar;
  int       pos;
  int       row, col;

  RotateCH(c,RotCH);

  if (rowChar<0) rowChar=0; // Safe pointer
  if (rowChar>8) rowChar=8; // Safe pointer

  pos=31-posChar; // '31-' inserisce i caratteri a destra

  for(col=RotCH[0]-1;col>=0;col--)
  {
    maskMatrix=1L<<pos;
    maskChar  =1<<col;
    for(row=rowChar;row<(rowChar+8);row++)
    {
      if ((row<8)) // row=8-1; // Safe pointer
      {
        if ((RotCH[row+1]&maskChar)) MatrixArea[row] |=  maskMatrix;
        else                         MatrixArea[row] &= ~maskMatrix;
      }
      else 
      {
        break;
      }
    }
    if(pos>0) pos--; else break;
  }
  if (isAlphaNumeric(c) && pos>0)
  {
    maskMatrix=1L<<pos;
    for(row=rowChar;row<(rowChar+8);row++)
    {
      if ((row<8)) // row=8-1; // Safe pointer
      {
      if ((row+1)>8) row=8; // Safe pointer
      MatrixArea[row] &= ~maskMatrix;
      }
      else break;
    }
    if(pos>0) pos--;
  }
  
  return (31-pos);
}

int MatrixWriteString(char *s,int posString, bool CLEAR)
{
  int ptr, row;
  int len=strlen(s);

  if (CLEAR)
  {
    for(row=0; row<8; row++) MatrixArea[row]=0x0L;
    DispString[0]='\0';
  }

  strcat(DispString,s);

  for(ptr=0;ptr<len;ptr++)
  {
    posString=MatrixWriteChar(s[ptr], posString, 0);
    if(posString>=31) break;
  }

  CleanBarDisplay();

  return ptr;
}

void CleanBarDisplay(void)                  // Clean display
{                                        //
  byte     data_byte, addr;              // *
  uint32_t data_row;                     // *
                                         // *
  for(uint8_t row=0; row<8; row++)       // *
  {                                      // *
                                         // *
    for(addr=4; addr>0; addr--)          // *
    {                                    // * * * * * * * * * * * * * * * *
      data_row=MatrixArea[row];          //
      for(uint8_t x=0; x<addr; x++)          // Clean the bar display
      {                                  //
          data_byte=data_row % 0x100L;   //
          data_row/=0x100L;              //
      }                                  //
      lc.setRow (addr-1,row,data_byte);  //
    }
  }
}
//
//void PrintByte(byte number)
//{
//  int bn;
//  byte mask, bitExtract;
//
//  for(bn=7;bn>0;bn--)
//  {
//    mask=1<<bn;
//    bitExtract=number&mask;
//    if(!bitExtract) Serial.print("0"); else break; //
//  }
//  Serial.println(number,BIN);
//  return;
//}
//
//void PrintLword(uint32_t number)
//{
//  int bn;
//  uint32_t mask;
//  uint32_t bitExtract;
//
//  for(bn=31;bn>=0;bn--)
//  {
//    mask=1<<bn;
//    bitExtract=number&mask;
//    if(!bitExtract) Serial.print("0"); else break; //
//  }
//  for(;bn>=0;bn--)
//  {
//    mask=1<<bn;
//    bitExtract=number&mask;
//    if(bitExtract) Serial.print("1"); else Serial.print("0");  //
//  }
//  return;
//}

void RotateCH(int codeASCII, byte *RotCH) // Debug completato, OK
{
  uint8_t codeChar=codeASCII-0x20;
  uint8_t rowCHdata;
  uint8_t ptrCH, ptrRotCH; //, ptrRotCHbit;

  for(ptrRotCH=1; ptrRotCH<9; ptrRotCH++)  RotCH[ptrRotCH]=0;
  RotCH[0]=CH[codeChar][0];

  for(ptrCH=1; ptrCH<(CH[codeChar][0]+1); ptrCH++)
  {
    rowCHdata = CH[codeChar][ptrCH];
    for(ptrRotCH=1; ptrRotCH<9; ptrRotCH++)
    {
      RotCH[ptrRotCH]=(RotCH[ptrRotCH]<<1)+(rowCHdata%2);
      rowCHdata/=2;
    }
  }
  return;
}

// ## End of program ------------------------------------------------------------------------------
