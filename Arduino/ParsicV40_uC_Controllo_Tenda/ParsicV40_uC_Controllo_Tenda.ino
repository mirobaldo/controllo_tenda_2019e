//==============================================================================
#define VERSION_ "V1.190707.19a" // Sviluppo
//efine VERSION_ "V1.190622.12a" // Sviluppo
//efine VERSION_ "V1.190614.19b" // Sviluppo
//efine VERSION_ "V1.190314.19b" // Versione iniziale
//
// === Messages ================================================================
const char VERSIONmsg[]   PROGMEM  = {"$ -- VoltageController -- Version: "};
//
// === Timer interrupt =========================================================
#include <Timer.h>
Timer Tmr;
//
const uint8_t InterruptTime = 5;
// -----------------------------------------------------------------------------
//
uint8_t PosTest;
uint8_t PositionCommand;
bool    SendMsg;
int16_t DebugReg[14];
uint16_t DispTim=1000;
//
// === DEBUG ===================================================================
#include <Debug.h>
//
// === Serial messages from PROGMEM ============================================
#include "SerialPROGMEM.h"
serialPROGMEM SPM;
//
// === Definizione funzioni pin ================================================
struct pinDefinition
{
  byte D00;               // D00
  byte D01;               // D01
  byte TestSignal;        // D02
  byte WindSwitch_int;    // D03
  byte PosReed;           // D04
  byte DHTio;             // D05
  byte OpenIn;            // D06
  byte DownIn;            // D07
  byte OpenOut;           // D08
  byte DownOut;           // D09
  byte D10;               // D10
  byte D11;               // D11
  byte D12;               // D12
  byte D13;               // D13
};

/*******************************************************************************
Pin ControlloTenda:
const byte pinWindSwitch_int =  3; // Anemometro rosso, giallo a massa
const byte pinPosReed        =  4;
const byte pinDHT            =  5; // what digital pin we're connected to
const byte pinOpenIn         =  6;
const byte pinDownIn         =  7;
const byte pinOpenOut        =  8; // bianco
const byte pinDownOut        =  9; // verde
const byte pinLCLoad         = 10; // LedControl load pin
const byte pinLClock         = 11; // LedControl clock pin
const byte pinLCData         = 12; // LedControl data pin
*******************************************************************************/

const pinDefinition pin={0,1,2,3,4,5,6,7,8,9,10,11,12,13};
// === Timer ===================================================================
class UserTimer
{
private:
  long      mMilliSec;
  long      mMilliSec_set;
  uint16_t  mDecInterval;        
public:
//--- COSTRUTTORE di UserTimer ----------------------------------------------
  UserTimer(uint16_t DecInterval)
  {
      if(DecInterval<1000) mDecInterval=DecInterval;
      else                 mDecInterval=1;
      mMilliSec=0L;  
  }
// --- Decrement ------------------------------------------------------------
  bool Decrement(void)
  {
    if(mMilliSec<1L)
    {
      mMilliSec=mMilliSec_set;
      return(true); 
    }
    else
    {
      mMilliSec-=(long)mDecInterval;
      return(false);
    }
  }
  long Set(long TimerValue_mS)
  {
    if(TimerValue_mS<100000) mMilliSec_set=TimerValue_mS; 
    else                     mMilliSec_set=100000;
    return (mMilliSec_set);
  }
};
UserTimer TIMMSG(5);
// === SunBlind simulator ======================================================
#include "SunBlindSimulator.h"

byte pinref[6]={pin.PosReed, pin.OpenOut, pin.OpenIn, pin.DownOut, pin.DownIn, pin.WindSwitch_int};

SunBlindSimulator SBS(pinref, InterruptTime);

// === Prototipi ===============================================================
char    GetChar(void);       // Prendi comando
// === SETUP ===================================================================
void setup() 
{
  PosTest=0;

  PositionCommand=0;
  
  pinMode(pin.D00,            INPUT);
  pinMode(pin.D01,            INPUT);
  pinMode(pin.TestSignal,     OUTPUT);
  pinMode(pin.WindSwitch_int, OUTPUT); // 3 Controllo software emulazione
  pinMode(pin.PosReed,        OUTPUT); // 4 Controllo software emulazione
  pinMode(pin.DHTio,            INPUT);  // 5 Dispositivo hardware
  pinMode(pin.OpenIn,         INPUT);  // 6 Controllo pulsante PARSECV40 hardware
  pinMode(pin.DownIn,         INPUT);  // 7 Controllo pulsante PARSECV40 hardware
  pinMode(pin.OpenOut,        INPUT);  // 8 Controllo da microcontroller
  pinMode(pin.DownOut,        INPUT);  // 9 Controllo da microcontroller
  pinMode(pin.D10,            INPUT);  // 10 display load
  pinMode(pin.D11,            INPUT);  // 11 display clock
  pinMode(pin.D12,            INPUT);  // 12 display data
  pinMode(pin.D13,            INPUT);  // 13 LED (watchdog) 

  digitalWrite(pin.WindSwitch_int, LOW); // 3
  digitalWrite(pin.PosReed,        LOW); // 4
  
  // loop
  Tmr.every((long)InterruptTime, IntTim_ReadSwitch);

  TIMMSG.Set(DispTim);
  
  Serial.begin(115200);

  SPM.printPROGMEM(STARTmsg01a,false); Serial.println(VERSION_);
}

// === LOOP ====================================================================
void loop() 
{
  const char strCMD_BUFFER_SIZE=32;
  char strCMD[strCMD_BUFFER_SIZE];
  char inchr;
  uint8_t pointer = 0;
//Serial.print("> ");
  while (1)
  {
    Tmr.update();
//  Loop comandi
    inchr = GetChar();

    if (inchr > 0)
    {
      if (inchr != '\n')
      {
        strCMD[pointer] = inchr;
        if (pointer < (strCMD_BUFFER_SIZE - 1)) pointer++;
        strCMD[pointer] = '\0';
      }
      else if (pointer > 0)
      {
        Serial.print("> "); Serial.println(strCMD);
        if (strcmp(strCMD, "LBL") == 0) // 
        {
          Serial.print(". ");
          Serial.print("POSITc "); // reg[0]=mPositionCnt;  
          Serial.print("REEDPc "); // reg[1]=mReedPosCnt;  
          Serial.print("POSCMD "); // reg[2]=mPositionCommand;
          Serial.print("WNDcnt "); // reg[3]=mWindCnt;
          Serial.print("WNDPLc "); // reg[4]=mWindPlsCnt;
          Serial.print("WPLPLn "); // reg[5]=mWindPlsNum;
          Serial.print("WNDSIM "); // reg[6]=mWindSimValue;
          Serial.print("POSINT "); // reg[7]=mPosCntInterval;
          Serial.print("POSPIN "); // reg[8]=mPosReedPin;
          Serial.print("oOPENP "); // reg[9]=mOpenOutPin;
          Serial.print("iOPENP "); // reg[10]=mOpenInPin;
          Serial.print("oDOWNP "); // reg[11]=mDownOutPin;
          Serial.print("iDOWNP "); // reg[12]=mDownInPin;
          Serial.print("WNDPIN "); // reg[13]=mWindSwitchPin;
          Serial.println(" ");
        } else 
        if (strcmp(strCMD, "STAT") == 0) // 
        {
          for(int i=2;i<12;i++)
          {
            Serial.print(". PIN");
            SPM.DecRJ(i,2,'0',false);
            Serial.print(": ");
            Serial.println(digitalRead(i)); 
          }
        } else 
        if (strcmp(strCMD, "TURN+") == 0) // 
        {
          PosTest=SBS.TESTOPEN;
          SPM.printPROGMEM(SBSTOmsg01a,false); 
          Serial.print(" ["); Serial.print(PosTest); Serial.println("]");  
        } else 
        if (strcmp(strCMD, "TURN-") == 0)
        {
          PosTest=SBS.TESTDOWN;
          SPM.printPROGMEM(SBSTDmsg01a,false); 
          Serial.print(" ["); Serial.print(PosTest); Serial.println("]");  
        } else 
        if (strcmp(strCMD, "TURNX") == 0)
        {
          PosTest=SBS.TEST_OFF;
          SPM.printPROGMEM(SBSTXmsg01a,false); 
          Serial.print(" ["); Serial.print(PosTest); Serial.println("]");  
        } else 
        if (strncmp(strCMD, "TMSG", 4) == 0)
        {
          unsigned int spd;
          int success = sscanf(strCMD, "TMSG:%u", &spd);
          if(success==1) 
          {
            DispTim=spd;
            SPM.printPROGMEM(CWINDmsg01a,false); 
            Serial.print("["); Serial.print(TIMMSG.Set(DispTim)); Serial.println("]"); 
            
          }
        }else        
        if (strncmp(strCMD, "WIND", 4) == 0)
        {
          unsigned int spd;
          int success = sscanf(strCMD, "WIND:%u", &spd);
          if(success==1) 
          {
            SBS.WindSpeed((uint8_t)spd);
            SPM.printPROGMEM(CWINDmsg01a,false); 
            Serial.print("["); Serial.print(SBS.ReadWindPlsNum()); Serial.println("]"); 
          }
          else
          {
            success = strcmp(strCMD, "WIND");
            if(success==0) 
            {
              SPM.printPROGMEM(CWINDmsg02a,false); 
              Serial.println(SBS.ReadWindPlsNum()); 
            }
            else
            {
              SPM.printPROGMEM(ERPARmsg01a,true);
            }
          }
        }
        else
            SPM.printPROGMEM(ERSTXmsg01a,true);          
        pointer = 0;
        strCMD[0] = '\0';
//      Serial.print("> ");
      }
    }
    if(SendMsg)
    {
      uint16_t n;
      Serial.print(". ");
      for(int i=0;i<14;i++)
      {
        SPM.DecRJ(DebugReg[i],6,'0',false); Serial.print(" ");
      }
//    Serial.print("SunBlindPos:");  n=SBS.ReadPosition();   Serial.print(n); Serial.print(" ");
//    Serial.print("ReedPos:");      n=SBS.ReadReedStatus(); Serial.print(n); Serial.print(" ");
//    Serial.print("PositionCommand:");  n=PositionCommand;  Serial.print(n); Serial.print(" ");
//    Serial.print("PosTest:");      n=PosTest;              Serial.print(n); Serial.print(" ");
      Serial.println(".");
      SendMsg=false;
    }
  }
}

// === INTERRUPT ===============================================================
void IntTim_ReadSwitch() // Routine di servizio timer
{
// digitalWrite(pin.TestSignal,LOW);
  
  SBS.WindHandler(); // Attivazione periodica simulatore anemometro
  
  int regPositionCommand=SBS.PositionHandler(PosTest); // Attivazione periodica posizione tenda

  SBS.DebugReadReg(DebugReg);

  if(TIMMSG.Decrement()) 
  {
    SendMsg=true; 
    PositionCommand=regPositionCommand;
  }

  PosTest=SBS.TESTIDLE;
}

/*-----------------------------------------------------------------------*
 * Lettura comandi                                                       *
 *- ---------------------------------------------------------------------*/
char GetChar() // --§--
{
  char c = 0;
  if (Serial.available() > 0)
  {
    c = toupper((char)Serial.read());
  }
  return (c);
}
